### MiniX 4
![](scam.png)
![](scam2.png)
![](scam3.png)

## A link to a Youtube-video of my work:
https://www.youtube.com/watch?v=mofEbRmqowA

## Link to RunMe:
https://rosalineforever.gitlab.io/aestheticprogramming/MiniX4

## Link to code:
https://gitlab.com/rosalineforever/aestheticprogramming/-/blob/main/MiniX4/sketch.js


## ReadMe

## Title: "It’s a scam, stupid"
This artwork is strongly inspired by the online scammers of the internet, and emphasizes the importance of general data protection regulation, GDPR. I am of the belief that all smartphone and PC users are either aware of, or have experienced the phenomenon of “scam”. What I associate with scam is; poorly created websites, ads with a text like “you are the lucky winner of an IphoneX”, and sketchy emails from random people you do not know, who want you to send them money or click on encrypted links. These scammers are everywhere on the internet now; in your email inbox, in your direct messages on Facebook and Instagram and in ads on websites in which you navigate every day. 
This artwork allows the user to press two different buttons; “Yes, I agree with all terms and conditions” or “No, I do not agree”. The terms-and-conditions-text is very long and written in small light gray lower case letters. This is done, to move focus from the long text, to the title and the interactive buttons. If the user decides to not agree with the terms and conditions, an inactive button will appear with a short text about how in the world of software, you have to agree with certain terms and privacy policies, in order to access given websites. This is a strong encouragement to go back and agree. If the user decides to agree, the person will discover that this actually is a “scam-trap”, and that the terms-and-condition-text involves some crazy conditions.

## The program
What I have had the most use of in this week’s MiniX, is creating my own functions  (“function faceCapture();” and “function tooBad();”) and using these in the mousePressed-syntax. This made me able to imitate various web pages on an “imaginary” website. Making my own functions made the coding a lot more manageable, since I could split up the coding into sections, and work in one section at a time.  
I have also worked with different DOM-elements. The first being buttons, and not only as interactive elements like the agree-to-all-terms-and-conditions-button, but also as text-fields for personal aesthetic reasons. Another DOM-element I have used is the video capture, which captures a live video via the webcam, and displays it on the screen. Another new syntax I have used is the “button.hide();”. This syntax allows you to hide different elements throughout your coding - in this case; hiding the two buttons from the first page, when either agreeing or disagreeing with the terms and conditions. One element in my coding that I am not so pleased with, is how I have made the terms-and-conditions-text “disappear” when entering the two different sites from clicking on the buttons. My solution is making a long white rectangle that covers the text when entering the two other sites. 

## Conceptually 
How I think my program and thinking address the theme of “capture all”, is in my attempt to imitate how scammers on the internet easily have gotten access to a lot of different personal information and sensitive data, and how we as users continue to feed our data in return for acces to given sites and targeted acvertising, that benefits us and makes life on the internet a bit easier. This is possible because users of these modern smart devices are able to track so many aspects of their lives - from menstrual cycles, sleep, food intake and steps per day. In ‘Aesthetic Programming, A Handbook of Software Studies’ (Soon & Cox, 2020) there is an example of this diverse and constant tracking: “Voice and audio recordings save a recording of your voice and other audio inputs in your Web & app activity on Google services and from sites, apps and devices that use or connect to Google speech services.” (p. 114). My program also highlights the never-ending "acceptance of cookies", when navigating on the World Wide Web. If you don't instantly agree and accept the given cookies, rules or conditions, it usually gets pretty complicated to e.g. access a given website. 
If the wrong people come into possession of all this data, it can have huge consequences. This is what I have tried to show in this MiniX - if you do not read what is written in small print, like the terms and conditions, you have no idea how much information you are giving up - and in exchange for what?.   

Reference list:
- Winnie Soon & Geoff Cox, 'Aesthetic Programming', 2020.
