let sad = 'I AM NOT OKAY RIGHT NOW!';
let happy = 'I AM PRETTY OKAY RIGHT NOW';
let both = 'Put your mouse to the right side if you are happy and the left side if you are sad';
let both2 = '(Press the mouse for music)';
let sadweb = 'https://Iwanttobehappyagain.com';
let happyweb = 'https://Ilovebeinghappyrightnow.com';

//declaring and defining the sad quotes:
let quote1 = '"never put you happiness in someone elses hands"';
let quote2 = '"I feel sad, but I do not know why"';
let quote3 = '"let it all out"';
let quote4 = '"tears are words that the heart can not express"';

//declaring and defining the happy quotes:
let happyq1 = '"do more of what makes you happy"';
let happyq2 = '"a smile is the shortest distance between two people"';
let happyq3 = '"do whatever makes your soul happy"';
let happyq4 = '"being happy never goes out of style"'

//declaring the music piece:
var song;

//uploading all the images:
function preload(){
//sad images:
  sad1 = loadImage('sad1.png');
  sad2 = loadImage('sad2.png');
  sad3 = loadImage('sad3.png');
//happy emoji-faces:
  happy1 = loadImage('happy1.png');
  happy2 = loadImage('happy2.png');
  happy3 = loadImage('happy3.png');

//webpage images:
  web1 = loadImage('web1.png');


//uploading all the gifs:
  gif1 = loadImage("sad1.gif");
  gif2 = loadImage("thunder.gif");
  gif3 = loadImage("lonely.gif");
  gif4 = loadImage("snoop.gif");
  gif5 = loadImage("happy2.gif");
  gif6 = loadImage("dog.gif");

//uploading the music piece:
  song = loadSound("happy2.mp3");

}


function setup(){
createCanvas(windowWidth, windowHeight);

// setting the frame rate and inizializing the values of r, g and b, t, j and k, and of f, v and d:
frameRate(1000);
//declaring and defining some varibles used later in the code for coloring:
t = random(350);
j = random(350);
k = random(350);

f = random(450);
v = random(450);
d = random(450);
}

//Making a function mousePressed in order to control whether you want the music to be there or not:
function mousePressed(){
  if (song.isPlaying()){
    song.stop();
  }else{
    song.play();
  }
}

function draw(){

if(mouseX < 600){
  background(t, j, k);

//making it look like a webpage:
  fill(200);
  rect(0,0, windowWidth, 50);
  fill(255);
  rect(130,8,700,35);
  image(web1, 5, 10, 100, 30);
  fill(0);
  textSize(15);
  text(sadweb, 135, 20, 300, 40);
//sad emojis:
  fill(random(255),random(255),random(255));
  strokeWeight(4);
  ellipse(200, 490, 200, 200);
  image(sad1, 125, 420, 150, 150);
  ellipse(610, 420, 150, 150);
  image(sad2, 535, 350, 150, 150);
  ellipse(970, 200, 200, 200);
  image(sad3, 860, 100, 210, 210);
//sad gifs:
  image(gif1, 100, 150, 200, 200);
  image(gif2, 1100, 100, 200, 200);
  image(gif3, 600, 100, 200, 100);
// sad quotes:
  fill(random(455),random(455),random(455));
  textSize(18);
  textFont('Georgia');
  text(quote1, 320, 520, 500, 200);
  text(quote2, 500, 300, 500, 200);
  text(quote3, 200, 100, 500, 200);
  text(quote4, 800, 500, 500, 200);
//text in the buttom of the canvas:
  textSize(50);
  fill(0);
  text(sad,300,600,800,200);
  textSize(23);
  text(both, 300, 700, 900, 300);
  textSize(18);
  text(both2, 300,740,900,300);

}

if(mouseX > 600){
  background(f, v, d);
//making it look like a webpage:
  fill(200);
  rect(0,0, windowWidth, 50);
  fill(255);
  rect(130,8,700,35);
  image(web1, 5, 10, 100, 30);
  fill(0);
  textSize(15);
  text(happyweb, 135, 20, 300, 40);
//happy emojis:
  fill(random(255),random(255),random(255));
  strokeWeight(4);
  ellipse(200,300,200,200);
  image(happy1,130,240,130,130);
  ellipse(720,150,150,150);
  image(happy2,665,110,100,100);
  ellipse(1100,330,200,200);
  image(happy3,1025,270,150,140);

//happy gifs:
  image(gif4, 500, 100, 100, 200);
  image(gif5, 100, 600, 150, 200);
  image(gif5, 1150, 600, 150, 200);
  image(gif6, 700, 350, 200, 200);
//happy quotes:
  fill(random(455),random(455),random(455));
  textSize(18);
  textFont('Georgia');
  text(happyq1, 200, 500, 500, 200);
  text(happyq2, 500, 300, 500, 200);
  text(happyq3, 200, 100, 500, 200);
  text(happyq4, 920, 500, 500, 200);
//text in the buttom of the canvas:
  fill(0);
  textSize(50);
  text(happy,300,600,800,200);
  fill(0);
  textSize(23);
  text(both, 300, 700, 900, 300);
  textSize(18);
  text(both2, 300,740,900,300);
  }
}
