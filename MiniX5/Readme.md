## MiniX5

# Link to a Youtube-video of my work:
https://www.youtube.com/watch?v=X-XTzt3k8qA

# Link to the code:
https://gitlab.com/rosalineforever/aestheticprogramming/-/blob/main/MiniX5/sketch.js

# link to RunMe:
https://rosalineforever.gitlab.io/aestheticprogramming/MiniX5/


![](loveletter.png)

### ReadMe
## Emotion Calendar
What I wanted to imitate in this MiniX5, is the first thing that I thought about when I thought of something being auto generated. In my opinion, people are able to control so many aspects and things in their daily lives, and when talking about coding, this is usually thought of as something very specific and controlled aswell. Though, one particular thing comes to my mind when I think of something the human brain can not always control and/or comprehend; emotions in general, mournful feelings, hate and of course; love. My MiniX is a love-calendar that will decide your mood for the month. Everyone knows what it feels like to be lost in own thoughts and feelings, and having a want to just pass responsibilities and decisions on to someone else to deal with. Especially during hard times. My MiniX decides that for you, and underlines how normal it is to vary in the emotional spectrum.

## I decided to implement these rules into my program:
1) The program can only generate two different “feelings” about someone; either “I HATE YOU” or “I LOVE YOU” on that certain date.
2) Each date/square in the grid has to have a random color every time the page is refreshed
3) All of the above have to fit within each square in the grid, in order to make it look like a calendar

## How the program performs over time
The way in which my program performs over time is very rapid, since I wanted the calendar to be instantly “finished” when you refreshed the page or "logged into" your calendar. I wanted to highlight the randomness of a “human-mind-calendar”, since the human mind is so complex and random - and therefore, I chose to randomize the days in which the user resp. “love you” and “hate you”, as well as randomize the colors of the squares in the grid. I chose to implement the random colors (white to black) to make these colors a symbol of “further” or “added” feelings of that certain day. White being happiness and calmness, black being anger and sorrow. Sometimes the grid/date will say “I love you” with a black background, which at first, I found a bit inconvenient. Though I actually ended up thinking that this contrasting color to the expressed feeling, gives a more nuanced picture of love and hathred, and how it can differentiate and vary. I also wanted the calendar to be different each time the user would open it, to make it an example of how our feelings can switch and differentiate throughout a day/month/year. I chose “love you” or “hate you” because these thoughts and beliefs are so strong and emotional. These very strong statements might seem a bit far out or bipolar, but again; exaggeration clarifies the understanding. 

## Understanding of the idea of "auto-generator"
When working on my MiniX, I was strongly inspired by Christopher Strachey’s love-letters algorithm from 1953. Strachey's work is an example of generative art, where certain rules in the program dictate what feelings are expressed in the love letter. This certain element is something I have tried to implement in my own work - both the technical but also the conceptual. Another thing that crossed my mind while working on my MiniX, is how I have chosen the form of randomness called “formal” indeterminacy in order to imitate the “social” indeterminacy, as Nick Montfort et al. writes about in the book ‘10 PRINT’ (2012). In this book, formal indeterminacy is referred to as “any form of random allotment, which often can be understood and modeled through statistical methods" (p. 123, Montfort et al) - which I find to be in relatively close relation to the way in which we code and program. To me, this formal indeterminacy is in clear contrast to the social indeterminacy, which is more about the social human mind and trying to understand how and why other people behave and think like they do (Montfort et al). I really like how this is such a big contrast, though still listed under the category “forms of randomness” - pretty similar to our feelings of love and hate. 

# Reference list: 
- Nick Montfort et al.; ‘10 PRINT CHR$(205.5+RND(1))’, (2012) 
- Siobhan Roberts, “The New Yorker”, (2017)

