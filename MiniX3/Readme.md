### MiniX 3
![](saturn.png)

## A link to a Youtube-video of my work:
https://www.youtube.com/watch?v=mofEbRmqowA

## Link to RunMe:
https://rosalineforever.gitlab.io/aestheticprogramming/MiniX3

## Link to code:
https://gitlab.com/rosalineforever/aestheticprogramming/-/blob/main/MiniX3/sketch.js


## ReadMe

### What I wanted to explre and express
What I wanted to explore in this week’s MiniX, was the transformational function, rotation, using arrays and exploring time. Also, I wanted to create something simple but still kind of mesmerizing and colorful… and fun. In one lecture we talked about people's perception of time, and how throbbers make it look like the computer is “doing” something behind the screen. When making a throbber, you are automatically designing an experience for the user, and as Hans Lammerant writes in ‘How humans and machines negotiate experience of time’ from 2018; “any experience is linked with an experience of time” and that this experience is “shaped strongly by all sorts of design decisions” (both: Lammerat, 2018). Also, it is mentioned in the book 'Aesthetic Programming' written by Winnie Soon and Geoff Cox, how; "Loops offer alternative imaginaries of time" (p. 92). My loop does not reveal how much time there is left for the user to wait. It could loop forever. In this way, the throbber is a very interesting thing to work with, since you are designing a waiting experience for an end-user, and you do not know whether this person likes to be entertained and exposed to colors and interaction, or if the person just wants something simple and effortless. Therefore I chose a middle way - I have created a colorful and "cute" throbber that represents the sun in the middle of the canvas, and Saturn that has a rotation around the sun. I made the background look like a sky with stars, and i wrote the text: "Saturn making it look like something is about to happen..." which I placed above the throbber. This text is supposed to be a reference to the theme about computional time vs. "real" time. 

### Time related syntaxes and how time is being constructed
As Hans Lammerat mentions in his book 'How Humans and Maschines Negotiate Experience of Time', time can be so many different things; day and night time cycles, calendar systems and even the speed of light. He also mentions maschine’s natural cycle, and how “all PC’s have a Real Time Clock (RTC) independent of the processor” (Lammerat, 2018). When I have been dealing with time in this week’s MiniX, I have paid special attention to how p5.js works and the time-functions within this program. 
I am aware that when you make syntaxes within the “function draw();”, these syntaxes will be run through 60 times in just one second - in other words; there is shown 60 frames per. second. To make Saturn rotate around the sun in a circular motion, I added the “angle += 0,017”. This will also be run through 60 times per. second, since I placed it within the “function draw();”. Every time the program runs through the code, an extra 0,017 will be added to the variable “angle”. When you rotate objects in p5.js, it will be rotated in degrees, and when using the syntax “rotate();”, the syntax uses the variable “angle” to know how many degrees to rotate with, every second. Time-wise this means that every 60th-part of a second the variable “angle” and the rotate-syntax will be updated, so that Saturn moves 0,017 degrees more. Personally I find it very interesting to work in milliseconds, since you have to be very precise and use the right decimal numbers, in order to make the rotation look good in the throbber. 

### A throbber that I have encountered before
A throbber that I clearly remember and really like, is the little jumping dinosaur on Chrome's browser that appears when the internet connection is lacking. This throbber is interactive; when you press the arrows on the keyboard, you are able to control the animated dinosaur and make it jump over animated obstacles on the screen. It is a while-you-are-waiting-for-a-better-internet-connection-game, which I personally feel like makes time pass by faster. I find this throbber very interesting, innovative and fun, in contrast to e.g. normal and boring throbbers that consist of small circles that turn around in circular motions or something similar to that. I know that they are simple, and that the coding behind them is more complex than people might think, but if I were to decide, I would add more colors and more playfulness in the design. Especially when many people experience and/or interact with these throbbers several times during the day. With this being said, I also think that there are exceptions. I would not want to play a dinosaur-game when watching Netflix or when buying an airplane ticket for a vacation - in these cases I have a very clear goal, and want time to pass by as fast as possible. In these situations I prefer the simple throbbers. 

Reference list:
- Hans Lammerat, 'How Humans and Maschines Negotiate Experience of Time', 2018. 
- Winnie Soon and Geoff Cox, 'Aesthetic Programmning', 2020




