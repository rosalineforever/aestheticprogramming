#### MiniX2 ReadMe
![](emojis.png)

## A link to a Youtube-video of my work:
https://www.youtube.com/watch?v=tYWUOOBRaKM

## Link to RunMe:
https://rosalineforever.gitlab.io/aestheticprogramming/MiniX2

## Link to code:
https://gitlab.com/rosalineforever/aestheticprogramming/-/blob/main/MiniX2/sketch.js

## ReadMe 

### My program - what I have used and learnt
What you see when you first experience my MiniX2-design, are two very simple looking emojis. The two emojis are filled with random colors - and in order to change the colors, you need to refresh the page. Besides that, both emojis have eyes, only consisting of the outline and the pupils - there is no actual “eye color” or color of the iris. My main focus in the MiniX2, has been to make the emojis as universal and including as possible. That's why I chose to randomize the skin colors, and give them no eye color. Also, I made a brush effect/function, to give the user the ability to decide almost all the facial expressions of the emojis. I was in doubt about which facial features would be universal for every user - and I did not find a solution for that. Though, I did not want the emojis to be completely blank, and instead, I gave them both a set of cartoonish eyes, to distance them from real human eyes. I also have to mention the fact that the two emojis are looking at each other. I wanted to make a clear connection between them. Also, I did this to make the user consider the facial duets people have in relation to each other, and which emotions/expressions from one emoji will result in emotions/expressions in the other. 
How I made this, was starting off by declaring resp. “r”, “g” and “b” with the syntax “let”. Besides setting the canvas and making the background, I defined my variables  “r”, “g” and “b” with colors between 0 and 150 with “random(150)”. I then used these colors to fill the two emojis with “fill(r, g, b)”. A new thing that I have done in the Minix2, is using an if-statement - also a conditional statement: “if(mouseIsPressed)”. The if-statement fills a little ellipse that follows the cursor (ellipse(mouseX, mouseY, 2, 2). In this way, the little ellipse is filled with black, if the mouse is pressed, and this brush effect will appear at the coordinates of the cursor, when it is pressed. Other than that, I have used new variables - e.g. “mouseX” and “mouseY”. The other elements in this miniX consist of ellipses. 

### How I would put my emoji into a wider social and cultural context 
The way I would put my emojis into a wider social and cultural context, is raising the question about race and diversity in human appearances. To me, the randomized colors represent a mood or emotions rather than an actual skin color, since the random colors mostly are unrealistic in terms of skin colors (purple, green, blue and dark gray). I like that the user is able to express her/him/oneself through the “brush effect” in my MiniX2, and decide what exact emotions, facial expressions and features the two emojis should have - and in relation to “each other”. I was really struggling about whether I should do two very detailed and aesthetic-looking emojis, or whether I should make the statement and message clearer, and make something very simple instead. I chose to go with the last one.
Another thing I had in mind was to make the emojis a bit more friendly looking, than the AImojis presented at the ‘Vienna Biennale for Change 2019’. They creeped me out - and I know that the message was the main focus of this event - I just would not use them in practice or in my own life. In the book 'Aesthetic Programming' written by Winnie Soon and Geoff Cox, it is mentioned how much complexity there is in human emotions, and how the face does not necessarily reflect the emotions of the given person; “Facial recognition systems also become unreliable when facial expressions vary, and even a big smile can falsify the result” (p. 67). 
To me, it is important that you as a user are able to customize features for yourself, in the software that you use on a daily basis. It is important to feel represented and comfortable, instead of settling for generalizing and sometimes discriminating things. This ability to customize software is trending right now. An example I was inspired by, is the app ‘Bitmoji’, that allows the user to customize every single little detail of their own emoji-avatar, with the intention to make it look like themself.  You can then use these emoji-avatars when texting, and it works for both Android and Iphone.


Reference list:
- Vienna Biennale for Change, "UNCANNY VALUES", 2019. 
- 'Aesthetic Programming', Winnie Soon & Geoff Cox, 2020.



