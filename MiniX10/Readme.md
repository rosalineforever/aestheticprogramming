## MiniX10

![](hand.png)

### ReadMe:
Which sample code have you chosen and why?
We have chosen the sample code “Handpose”.

https://learn.ml5js.org/#/reference/handpose

We chose it because it was interesting to take a look at something other than capturing a face. We wanted to choose another body part, and see what the creator chose as key points when capturing a hand. Additionally we found it appealing to choose something where interaction was in focus. 
Besides that we talked about how a hand can show a lot of gestures, sending signals which we think is kind of different from face gestures. There are many ways in which we have seen hand gestures be the center of a bigger theme, an example is the peace sign, or when you are diving; there are a ton of different hand signs to communicate with, when being under water. So all in all, we like the fact that the gesture of the hand can be seen in different ways for a human and a computer.  

### Have you changed anything in order to understand the program? If yes, what are they?
In order to understand the coding better, we tried to; change the sizes of canvas and image, to change the sizes and colors of the ellipses that constitute the key points. We also tried to change the amount of keypoints, but it was instantly obvious that minimizing the amount of key points made the capturing (the coherent keypoints) seem less like a hand capture-function when experiencing the RunMe

### Which line(s) of code is particularly interesting to your group, and why?
We found a couple elements of the code interesting, as there were some new syntaxes that we were not familiar with. Specific lines of code that we looked at:
function drawKeypoints() {. This we found interesting as it links to the points of the fingers that are shown as green dots. It was a bit hard to understand where the function “got the information” from, but we figured out that the key points were located in another .js file.
handpose.on("predict", function(results) { We found this line quite difficult to understand, as we have not seen the “predict” syntax before. We spent some time looking at what it means, and found out that when the predict function is called, it tells the handpose what to do with the results
function (preload): img = loadImage("data/hand.jpg"); We also found it interesting that an image of a hand is being loaded, even though the picture of the hand is never being shown when the code is running. 



### How would you explore/exploit the limitation of the sample code/machine learning algorithms?
As mentioned above we tried to change the amount of keypoints, to see what would happen.  As mentioned the hand features are being more visible the more keypoint there are. The hand is less visible, because the green keypoint fills out the entire hand. 
Another thing we talked about is that the algorithm is based on only one photo of a hand, which we think is not representativt, but on the other hand the code works when seeing other hands. So maybe it is not a problem that there is only one photo.   

### What are the syntaxes/functions that you don't know before? What are they and what have your learnt?
Some of the syntaxes mentioned in the “code we found particularly interesting”-section are syntaxes we have not before tried ourselves. We also found the arrays used in the source code quite interesting. The array with the setup of the handcapure was especially interesting to look at. Here the points are divided up in; thumb, indexFinger, middleFinger, ringFinger, pinky and palmBase. In this way, the code depends on whether the person using the capture has a hand with all five fingers or not. When we tried to “remove” a finger by bending it down in order to make it be absent from the capture. When doing this, the key points are centered to the palm of the hand, since the hand capture does not capture the missing finger. 

### How do you see a border relation between the code that you are tinker with and the machine learning applications in the world (e.g creative AI/ voice assistants/driving cars, bot assistants, facial/object recognition, etc)?
The code we have chosen to tinker with can be related to hand gesture recognition. An example where you can see this is the PlayStation game, EyeToy. Here, a webcam registers the way you move your hands and wrist and base the result on this.
Another example that is related to the theme is the “Samsung Smart TV Gesture Control”, which allows the user to interact with and control the TV via hand gesture. This might be the future for all TV’s. 
We also think that the computer/AI will have trouble recognizing all the different hand gestures, because the hand gestures can be comprehended differently in cultures. 

### What do you want to know more/ What questions can you ask further?
We would like to see how the syntax would change, if there were to be two hands on the screen. 
Also, it would be interesting to dive into the world of “recognition” regarding both face and hands, as we have many questions as to how this works, as well as the problems that can/will follow.
Is interaction with technologies solely going to be based on hand tracking (instead of direct touch)? Watch this video, from 9:07-9:21. https://www.youtube.com/watch?v=96AO6L9qp2U
To look at a wider perspective, we wonder whether both face, body and hand recognition will be something that plays a bigger role in our everyday lives. Could it help to stop identity theft, if technology in the future is based on personal recognition?

