let x = 80;
let y = 80;
function setup() {
  // setting a canvas size
  createCanvas(800, 800);
}

function draw() {
  // setting a dark blue background color
  background(30, 30, 140);

  // making the "ground" for my catarpillar
  fill(120, 90, 80);
  rect(0.1, 300, 800, 500);

  // making a full moon that moves
  fill(250, 250, 60);
  ellipse(x, y, 65, 65);
  x= x+0.1;

  // drawing multiple green circles on the canvas
  fill(150, 290, 120)
  ellipse(315, 175, 36, 36);
  ellipse(325, 200, 42, 42);
  ellipse(345, 225, 44, 44);
  ellipse(360, 250, 46, 46);
  ellipse(380, 270, 48, 48);
  ellipse(385, 300, 50, 50);
  ellipse(370, 325, 52, 52);
  ellipse(350, 350, 54, 54);
  ellipse(340, 380, 56, 56);
  ellipse(350, 410, 58, 58);
  ellipse(370, 440, 60, 60);
  ellipse(395, 465, 62, 62);
  ellipse(420, 490, 66, 66);
  ellipse(460, 490, 68, 68);
  ellipse(480, 460, 70, 70);
  ellipse(495, 420, 72, 72);
  ellipse(510, 375, 86, 86);

  // giving the tentacles a green color
  fill(150, 290, 170);

  // drawing the left tentacle
  ellipse(490, 335, 10, 10);
  ellipse(488, 328, 10, 10);
  ellipse(488, 320, 10, 10);
  ellipse(490, 312, 10, 10);

  // drawing the right tentacle
  ellipse(548, 350, 10, 10);
  ellipse(554, 345, 10, 10);
  ellipse(558, 339, 10, 10);
  ellipse(560, 332, 10, 10);

  // giving the caterpillar a pink/red colored nose with black nostrils
  fill(400, 100, 120);
  ellipse(510, 380, 20, 20);
  fill(0, 0, 0);
  ellipse(504, 382, 3, 3);
  ellipse(513, 385, 3, 3);

  // giving the caterpillar white eyes
  fill(800, 800, 800);
  ellipse(495, 360, 15, 15);
  ellipse(532, 368, 15, 15);

  // giving the caterpillar black pupils
  fill(0, 0, 0);
  ellipse(495, 362, 5, 5);
  ellipse(532, 370, 5, 5);

  // giving the caterpillar a little black mouth
  fill(0, 0, 0);
  ellipse(505, 400, 9, 9);

  // giving the catarpillar some small rainbow dots
  // Yellow dots
  fill(255, 300, 40);
  ellipse(320, 172, 5, 5);
  ellipse(393, 460, 7, 7);
  ellipse(350, 374, 6, 6);
  ellipse(370, 270, 5, 5);

  // pink dots
  fill(255, 24, 150);
  ellipse(320, 200, 5, 5);
  ellipse(370, 430, 8, 8);

  // dark blue dots
  fill(0, 0, 200);
  ellipse(342, 222, 7, 7);
  ellipse(335, 370, 6, 6);

  // light blue dots
  fill(35, 190, 300);
  ellipse(395, 300, 6, 6);
  ellipse(360, 445, 7, 7);

  // orange dots
  fill(200, 160, 100);
  ellipse(360, 240, 7, 7);
  ellipse(370, 320, 7, 7);
  ellipse(345, 405, 8, 8);

  // green dots
  fill(20, 250, 10);
  ellipse(390, 265, 6, 6);
  ellipse(350, 345, 6, 6);

}
