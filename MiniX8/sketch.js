function preload() { //Preload to load the json-files before loading the page

  //actions possible
  dataActions = loadJSON('actions.json');

  //humans/people/animals
  dataHumans = loadJSON('humans.json');

  //loading the image:
  png1 = loadImage("diary.png");
}
function setup() {

  //setting up the canvas and adding the image:
  createCanvas(windowWidth, 100);
  background(248, 181, 255);
  image(png1, 350, 10, 150, 80);

  //writing the title:
  textSize(30);
  textFont('Georgia');
  fill(255);
  stroke(0);
  strokeWeight(3);
  text("A to do list for today 💕", 10,70);

  //referring to the array "actions" within the actions json-file:
      var actions = dataActions.actions;
  //referring to the array "humans" within the humans json-file:
      var humans = dataHumans.humans;
  //For-loop to show 6 actions each time the page is loaded or button is pushed:
    for(let i =0; i < 6; i++) {
      randomAction = random(actions);
  //replacing the {0} in the json-file with a random human from the human-file, and making sure that {1} is replaced with something different in the sentence:
      createP(randomAction.replace('{0}', random(humans)).replace('{1}', random(humans)));

//Button to make a new to do list, if it is pushed:
//Refering to function reload
      let col = color(248, 181, 255);
      button = createButton("Give me a new to do list, thanks!");
      button.position(40, 400);
      button.style('color:white');
      button.style('background-color', col);
      button.style("font-size", "1em");
      button.mousePressed(reload);
    }
}
//making a button that reloads the page, each time the button is being clicked:
function reload() {
  location.reload();
}
