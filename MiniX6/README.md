# MiniX6
![](MiniX6.png)

### Link to code:
https://gitlab.com/rosalineforever/aestheticprogramming/-/blob/main/MiniX6/sketch.js

### Link to RunMe:
https://rosalineforever.gitlab.io/aestheticprogramming/MiniX6

## ReadMe
### How does the game/obejct works?
What I started with in this week's MiniX, was creating a lot of small objects, by first defining and making a class - in this case “class Mice” - and giving this object the ability/method of moving around in a limited area on the canvas. I wanted the objects to imitate something that would be chased by something else, so I decided to make the objects into little mice - I did this by giving the objects “cartoonish” characteristics of a mouse. Then I decided that the user of the game should be a cat chasing the little mice. To do this, I found an image of a cat online, and made this image be in the coordinates of the cursor at all times, to imitate the user being a cat chasing a mouse - and also this being the point of the game. To me, it was not vital that the game should be like a traditional win or loose game or “how many points can you score?”. I wanted to focus on creating the classes and including an interactive element, from scratch.  

### How i program the objects and their related attributes + methods in the game
The methods and attributes that I added to my objects is firstly the basic display on the screen - referred to the syntax “show()”. I also chose to make the objects move around on the canvas by using the syntax “move()” and making them move around randomly in a demarcated area with the coordinates -10 and 10 on both the x- and y-axes. I also used the syntax “contains()” in order to give the objects the ability of “being eaten” by the cat, when the cursor is within the property of the mice and clicked at the same time - “contains()” refers to the function mousePressed earlier in the coding, and in the mousePressed I used the “mice.splice” in order to hide/delete the mice from the screen, when being clicked (to imitate the cat eating them). 

### Characteristics of OOP and the wider implications of abstraction 
In my opinion, object oriented programming in terms of implications of abstraction, gives the user of the coding program the ability to customize certain elements of the code in a much more manageable way, than with just using functions and logic. As it is mentioned in Soon & Cox’s book ‘Aesthetic Programming: A Handbook of Software Studies’, the way in which to handle the complexity of an object is by: “[...] abstracting certain details and presenting a concrete model” (p. 145). What you put in the class of your object, applies throughout the entire code, which also helps decrease the complexity, by keeping the class and the rest of the code separate - though still being able to always refer to and use the class in the code. 

### A wider cultural context 
The way in which my game connects to a wider cultural context, is more in terms of the traditional and universal understanding of animal-behavior and the relation between an aggressive cat and a little scared mouse. This also draws parallels to themes like “survival of the fittest” and to the food chain in general. But looking at my game in the light of abstracting complex details, this week’s MiniX has a deeper complexity and shows more of the different layers in coding, than the earlier assignments have done. This week we got introduced to the previously mentioned “classes”, which makes it a lot easier to store data about specific elements in the coding and in this way abstracting this from the rest of the coding. Like in any other cultural action, which coding also bears the mark of, there is a certain degree of subjectivity in the act: “[...] we engage in the politics of this movement between abstract and concrete reality, which is never a neutral process” (p. 146 - Soon & Cox, 2020). To me, my own game is pretty politically- and socially acceptable - it is more of a child's game, than it is something created to raise questions about e.g. society. 

Reference list:
- Winnie Soon & Geoff Cox; 'Aesthetic Programming: A Handbook of Software Studies', 2020
