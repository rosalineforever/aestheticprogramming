//letting mice be equal to an array
let mice = [];
let b = '(CAT)CH ALL THE MICE!🐭';
let a = 'Click on them in order to EAT them!'
let q = '"squeak!"'

//Preloading an image
function preload(){
  img = loadImage('cat4.png');
}

function setup() {
//setting up the canvas to fill out the entire screen:
  createCanvas(windowWidth, windowHeight);
//making a for-loop:
//if the condition is that i is less than 150
//then i increases by 1
//the loop stops at 150 mice, since it would no longer be true
  for (let i = 0; i < 150; i++) {
    //the action that happens, when the coniditon is met
    let x = random(width);
    let y = random(height);
    let r = random(20, 30);
    let b = new Mice(x, y, r);
    //push means adding something to the array
    mice.push(b);
  }
}
//making a mousePressed function to delete elements from the array
//when an object is clicked
function mousePressed() {
  //looping through an array backwards 
  for (let i = mice.length - 1; i >= 0; i--) {
    if (mice[i].contains(mouseX, mouseY)) {
      mice.splice(i, 1); //deleting one(1) element from the array/mice.length(i)
    }
  }
}

function draw() {
  background(206, 190, 165);
  //a for-loop with the length of mice - a reference to the for-loop in the setup
  //instead of writing 150 again, it will adjust to the other for-loop
  for (let i = 0; i < mice.length; i++) {
    mice[i].move();
    mice[i].show();
}
  //making the cursor a picture of a cat:
    image(img, mouseX-45,mouseY-45);


  //drawing a text:
    fill(0);
    textSize(60);
    textFont('Georgia');
    text(b,150,600,900,400);

    textSize(20);
    text(a,200,680,700,400)

  //drawing the "squeaks":
    textSize(15);
    text(q,200,200,100,100);
    text(q,500,100,100,100);
    text(q,800,150,100,100);
    text(q,980,250,100,100);
    text(q,480,350,100,100);
}
// making the class for my object:
class Mice {
  //the objects "setup" - what does it mean to be a mouse?
  //"this" is a reference to the object instance
  //x, y and r are already defined in the function setup
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
  }


//refering to the mousePressed-function (making sure the cursor is within the object):
  contains(px, py) {
    let d = dist(px, py, this.x, this.y);
    if (d < this.r) {
     return true;
     } else {
     return false;
    }
  }

//the behaviour of the object
// making the mouse move around on the canvas:
  move() {
    this.x = this.x + random(-10, 10);
    this.y = this.y + random(-10, 10);
  }

//the properties of the object 
  show() {
  fill(211,211,211);
  stroke(95,95,95);
  ellipse(this.x, this.y, this.r * 2);
  ellipse(this.x-15, this.y-25, this.r * 0.8);
  ellipse(this.x+15, this.y-25, this.r * 0.8);
  fill(255, 139, 139);
  ellipse(this.x-15, this.y-25, this.r * 0.4);
  ellipse(this.x+15, this.y-25, this.r * 0.4);

  //eyes:
  stroke(0);
  fill(255);
  ellipse(this.x-10, this.y, this.r * 0.4);
  ellipse(this.x+10, this.y, this.r * 0.4);
  fill(0);
  ellipse(this.x-10, this.y, this.r * 0.1);
  ellipse(this.x+10, this.y, this.r * 0.1);
  ellipse(this.x, this.y+10, this.r * 0.2);

  }
}
