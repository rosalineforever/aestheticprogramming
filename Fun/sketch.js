let x = 2;
let y = 2;
//
let spacing = 60;

function setup() {
  // setting upå the canvas and giving it a background
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(10);
}

function draw() {
  stroke(0);
  fill(random(255));
// implement the rules with a conditional statement:
  if (random(1) < 0.8) {
//drawing the backward slash
    ellipse(x, y, x+spacing, y+spacing);

  } else {
//drawing the forward slash
   ellipse(x, y+spacing, x+spacing, y);

  }
  // making sure that when they reach the end, they have to go back and start from the beginning (x is constantly changing)
  x+= spacing;
// conditional statement:
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
