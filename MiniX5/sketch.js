//declaring and inizializing s, l and h:
let s = 'Today I will...';
let l = 'LOVE YOU❤️';
let h = 'HATE YOU🤢';

//declaring and defining x and y, together with cols and rows (used in the for-loop):
let x = 0;
let y = 0;

var cols = 10;
var rows = 8;

//declaring and defining spacing (used in the if-statement)
let spacing = 105;

//making two different arrays:
var d = ['1. april','2. april','3. april','4. april','5. april','6. april','7. april','8. april','9. april','10. april','11. april','12. april','13. april','14. april','15. april','16. april','17. april','18. april','19. april','20. april','21. april','22. april','23. april','24. april','25. april','26. april','27. april','28. april','29. april','1. may','2. may','3. may','4. may','5. may','6. may','7. may','8. may','9. may','10. may','11. may','12. may','13. may','14. may','15. may','16. may'];
var dd = ['17. may','18. may','19. may','20. may','21. may','22. may','23. may','24. may','25. may','26. may','27. may','28. may','29. may','30. may','1. june','2. june','3. june','4. june','5. june','6. june','7. june','8. june','9. june','10. june','11. june','12. june','13. june','14. june','15. june','16. june','17. june','18. june','19. june','20. june','21. june','22. june']

function setup() {
//setting up the canvas:
  createCanvas(windowWidth, windowHeight);
  background(255);

//making a for-loop in order to make the lattice/grid in the background
//let i equal 0. If i is smaller than cols, which is 10, i will increase by one
//let j equal 0. If j is smaller than rows, which is 8, j will increase by one
for(var i = 0; i < cols; i++){
  for(var j = 0; j < rows; j++){
    //if the condition above is met, this action will be executed:
    //draw 10*8 rectangles on the canvas with the width 100 pixels and the height 80 pixels
    //fill each grid with a random color 
   var x = i * 100;
    var y = j * 80;
    stroke(0);
    fill(random(400));
    rect(x,y,108,80); //the last two arguments decides the size of the wholde grid
  //including the "Today I will" text in the for-loop:
    fill(0);
    stroke(255);
    textSize(15);
    text(s,x+5,y+2,100,100);

//making an if-statement to generate the love-or-hate-statements 50/50:
//if a random number within 0-1 is smaller than 0.5, the "love" will be generated
//if else, "hate" will be generatedd
    stroke(255);
    if (random(1) < 0.5) {
      fill(350,100,100);
      stroke(2);
      textSize(15);
      text(l,x+5,y+17,spacing,spacing);

    } else {

      stroke(2);
      fill(43,255,0);
      text(h,x+5,y+17,spacing,spacing);
    }
  }
}
}

function draw(){
//drawing the dates in the squares in the grid:
  textSize(13);
  strokeWeight(1);
  stroke(255);
  fill(0);
  text(d[0],55,60,100,100);
  text(d[1],155,60,100,100);
  text(d[2],255,60,100,100);
  text(d[3],355,60,100,100);
  text(d[4],455,60,100,100);
  text(d[5],555,60,100,100);
  text(d[6],655,60,100,100);
  text(d[7],755,60,100,100);
  text(d[8],855,60,100,100);
  text(d[9],955,60,100,100);

  text(d[10],50,140,100,100);
  text(d[11],150,140,100,100);
  text(d[12],250,140,100,100);
  text(d[13],350,140,100,100);
  text(d[14],450,140,100,100);
  text(d[15],550,140,100,100);
  text(d[16],650,140,100,100);
  text(d[17],750,140,100,100);
  text(d[18],850,140,100,100);
  text(d[19],950,140,100,100);

  text(d[20],50,220,100,100);
  text(d[21],150,220,100,100);
  text(d[22],250,220,100,100);
  text(d[23],350,220,100,100);
  text(d[24],450,220,100,100);
  text(d[25],550,220,100,100);
  text(d[26],650,220,100,100);
  text(d[27],750,220,100,100);
  text(d[28],850,220,100,100);
  text(d[29],950,220,100,100);

  text(d[30],50,300,100,100);
  text(d[31],150,300,100,100);
  text(d[32],250,300,100,100);
  text(d[33],350,300,100,100);
  text(d[34],450,300,100,100);
  text(d[35],550,300,100,100);
  text(d[36],650,300,100,100);
  text(d[37],750,300,100,100);
  text(d[38],850,300,100,100);
  text(d[39],950,300,100,100);

  text(d[40],50,380,100,100);
  text(d[41],150,380,100,100);
  text(d[42],250,380,100,100);
  text(d[43],350,380,100,100);
  text(dd[0],450,380,100,100);
  text(dd[1],550,380,100,100);
  text(dd[2],650,380,100,100);
  text(dd[3],750,380,100,100);
  text(dd[4],850,380,100,100);
  text(dd[5],950,380,100,100);

  text(dd[6],50,460,100,100);
  text(dd[7],150,460,100,100);
  text(dd[8],250,460,100,100);
  text(dd[9],350,460,100,100);
  text(dd[10],450,460,100,100);
  text(dd[11],550,460,100,100);
  text(dd[12],650,460,100,100);
  text(dd[13],750,460,100,100);
  text(dd[14],850,460,100,100);
  text(dd[15],950,460,100,100);

  text(dd[16],50,540,100,100);
  text(dd[17],150,540,100,100);
  text(dd[18],250,540,100,100);
  text(dd[19],350,540,100,100);
  text(dd[20],450,540,100,100);
  text(dd[21],550,540,100,100);
  text(dd[22],650,540,100,100);
  text(dd[23],750,540,100,100);
  text(dd[24],850,540,100,100);
  text(dd[25],950,540,100,100);

  text(dd[26],50,620,100,100);
  text(dd[27],150,620,100,100);
  text(dd[28],250,620,100,100);
  text(dd[29],350,620,100,100);
  text(dd[30],450,620,100,100);
  text(dd[31],550,620,100,100);
  text(dd[32],650,620,100,100);
  text(dd[33],750,620,100,100);
  text(dd[34],850,620,100,100);
  text(dd[35],950,620,100,100);
}
