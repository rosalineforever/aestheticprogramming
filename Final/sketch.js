//Declaring and defining all quotes used in the program:
//The web-page
let search = 'www.mynormallifeaccordingtoGoogle.com';
let searches = 'Ca. 122.000.000 resultater (0,37 sekunder)';
let should = 'Should I...';

//Education
let e = 'You are normal! You chose to get an education.';
let ee = '95 percent of the students who leave 9th grade in 2015 must complete a youth education within 25 years. Likewise, 60 percent of those leaving 9th grade by 2020 must complete a higher education within 25 years, of which 25 percent a long higher education.';
let ee2 = 'You are abnormal! You chose to not get an education.';
let e2 = 'You are part of the 5% of primary school students who are not expected to take a higher education';

//Job
let jj = 'You are normal! You chose to get a job.';
let j = 'For the 16-64-year-old men, the employment rate was 76.5 % in 2018, while it was 72.6 % for women.';
let jj2 = 'You are abnormal! You chose not to get a job';
let j2 = 'For the 16-64-year-old men, the employment rate was 76.5 % in 2018, while it was 72.6 % for women.';

//Marriage
let mm = 'You are normal! You chose to get married.';
let m = 'About two million Danes are married, and about 1.6 million adults over the age of 18 live alone.';
let mm2 = 'Your are abnormal! You chose not to get married'
let m2 = 'About two million Danes are married, and about 1.6 million adults over the age of 18 live alone.';

//Children
let cc = 'You are normal! You chose to have children.';
let c = 'The birth rate in 2012 was 1.73 births per woman and in 2015 1.71 births per woman. This means that women in Denmark on average give birth to just under 2 children each.';
let cc2 = 'You are abnormal! You chose not to have children.'
let c2 = 'The birth rate in 2012 was 1.73 births per woman and in 2015 1.71 births per woman. This means that women in Denmark on average give birth to just under 2 children each.';

//Retirement
let rr = 'You are normal! You chose to retire at retirement age.';
let r = 'Three out of five pensioners receive a full national pension. in total, there are 1,145,000 old-age pensioners right now';
let rr2 = 'You are abnormal! You chose to retire earlier.'
let r2 = 'In January 2021, there were 228,000 early retirees. That is quite far from the 1,145,000 pensioners right now.';

//Death
let d = 'You are normal! You choose to die happy.';
let d2 = 'Every year, about one in ten, or about 5,000 people, die without family or friends around them.';
let dd = 'You are abnormal! You chose to die alone.';
let dd2 = 'Every year, about one in ten, or about 5,000 people, die without family or friends around them.';

//Preloading all images and sounds:
function preload (){
    //Background:
    img = loadImage("baggrund.png");
    img2 = loadImage("google.png");
    img3 = loadImage("searchField.png")

    //Sounds:
    educationSound = loadSound("education.mp3");
    noeducationSound = loadSound("noeducation.mp3");
    jobSound = loadSound("job.mp3");
    nojobSound = loadSound("nojob.mp3");
    marrySound = loadSound("marry.mp3");
    nomarrySound = loadSound("nomarry.mp3");
    childSound = loadSound("child.mp3");
    nochildSound = loadSound("nochild.mp3");
    nochildSound = loadSound("nochild.mp3");
    retireSound = loadSound("retire.mp3");
    earlierSound = loadSound("earlier.mp3");
    happySound = loadSound("happy.mp3");
    sadSound = loadSound("sad.mp3");
    clickSound = loadSound("click.mp3");


}
function setup() {
//setting up the canvas, and adding the background (bulletin board):
  createCanvas(1385, 1010);
  background(img);

//Google-logo and the search field:
  image(img2,30,35,155,60);
  image(img3,190,30,550,75);

//inserting other visuals and texts
  noFill();
  stroke(200);
  rect(5,10,1350,100);
  fill(0)
  textSize(17);
  text(search,220,58,350,30);
  fill(0);
  text(should,1050,200,100,40);
  textSize(15);
  fill(200);
  text(searches,40,125,350,100);


//The restart-button:
var button1 = createButton("🔍 Restart page");
let col = color(1000);
button1.style('background-color', col);
button1.size(120,40);
button1.position(775, 40);
button1.style("font-size", "0.9em");
button1.mousePressed(drawRestart);

//the other buttons:
var button2 = createButton("Get an education");
let col1 = color(1000);
button2.style('background-color', col1);
button2.size(125,50);
button2.position(1050, 250);
button2.style("font-size", "1em");
button2.mousePressed(drawEducation);

var button3 = createButton("Not get an education");
let col2 = color(1000);
button3.style('background-color', col2);
button3.size(125,50);
button3.position(1200, 250);
button3.style("font-size", "1em");
button3.mousePressed(drawNoeducation);

var button4 = createButton("Get a job");
let col3 = color(1000);
button4.style('background-color', col3);
button4.size(125,50);
button4.position(1050, 320);
button4.style("font-size", "1em");
button4.mousePressed(drawJob);

var button5 = createButton("Not get a job");
let col4 = color(1000);
button5.style('background-color', col4);
button5.size(125,50);
button5.position(1200, 320);
button5.style("font-size", "1em");
button5.mousePressed(drawNojob);

var button6 = createButton("Get married");
let col5 = color(1000);
button6.style('background-color', col5);
button6.size(125,50);
button6.position(1050, 390);
button6.style("font-size", "1em");
button6.mousePressed(drawMarry);

var button7 = createButton("Not get married");
let col6 = color(1000);
button7.style('background-color', col6);
button7.size(125,50);
button7.position(1200, 390);
button7.style("font-size", "1em");
button7.mousePressed(drawNomarry);

var button8 = createButton("Have children");
let col7 = color(1000);
button8.style('background-color', col7);
button8.size(125,50);
button8.position(1050, 460);
button8.style("font-size", "1em");
button8.mousePressed(drawChild);

var button9 = createButton("Not have children");
let col8 = color(1000);
button9.style('background-color', col8);
button9.size(125,50);
button9.position(1200, 460);
button9.style("font-size", "1em");
button9.mousePressed(drawNochild);

var button10 = createButton("Retire at the retirement age");
let col9 = color(1000);
button10.style('background-color', col9);
button10.size(125,50);
button10.position(1050, 535);
button10.style("font-size", "1em");
button10.mousePressed(drawRetire);

var button11 = createButton("Retire earlier");
let col10 = color(1000);
button11.style('background-color', col10);
button11.size(125,50);
button11.position(1200, 535);
button11.style("font-size", "1em");
button11.mousePressed(drawEarlier);

var button12 = createButton("Die happy");
let col11 = color(1000);
button12.style('background-color', col11);
button12.size(125,50);
button12.position(1050, 605);
button12.style("font-size", "1em");
button12.mousePressed(drawDiehappy);

var button13 = createButton("Die alone");
let col12 = color(1000);
button13.style('background-color', col12);
button13.size(125,50);
button13.position(1200, 605);
button13.style("font-size", "1em");
button13.mousePressed(drawDiealone);
}


function drawRestart(){
  location.reload();
}
function drawEducation(){
  educationSound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,190,700,60);
  fill(0,0,204);
  textSize(18);
  text(e,40,190,500,60);
  fill(100);
  textSize(14);
  text(ee,40,213,600,60);
}
function drawNoeducation(){
  noeducationSound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,160,700,110);
  fill(0,0,204);
  textSize(18);
  text(ee2,40,190,600,60);
  fill(100);
  textSize(14);
  text(e2,40,213,650,60);
}
function drawJob(){
  jobSound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,285,750,60);
  fill(0,0,204);
  textSize(18);
  text(jj,40,285,750,60);
  fill(100);
  textSize(14);
  text(j,40,310,600,60);
}
function drawNojob(){
  nojobSound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,285,400,60);
  fill(0,0,204);
  textSize(18);
  text(jj2,40,285,400,60);
  fill(100);
  textSize(14);
  text(j2,40,310,600,60);
}
function drawMarry(){
  marrySound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,365,750,60);
  fill(0,0,204);
  textSize(18);
  text(mm,40,365,750,60);
  fill(100);
  textSize(14);
  text(m,40,390,600,60);
}
function drawNomarry(){
  nomarrySound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,365,750,60);
  fill(0,0,204);
  textSize(18);
  text(mm2,40,365,750,60);
  fill(100);
  textSize(14);
  text(m2,40,390,600,60);
}
function drawChild(){
  childSound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,425,750,60);
  fill(0,0,204);
  textSize(18);
  text(cc,40,425,750,60);
  fill(100);
  textSize(14);
  text(c,40,450,600,60);
}
function drawNochild(){
  nochildSound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,425,750,60);
  fill(0,0,204);
  textSize(18);
  text(cc2,40,425,750,60);
  fill(100);
  textSize(14);
  text(c2,40,450,600,60);
}
function drawRetire(){
  retireSound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,500,750,60);
  fill(0,0,204);
  textSize(18);
  text(rr,40,500,750,60);
  fill(100);
  textSize(14);
  text(r,40,525,600,60);
}
function drawEarlier(){
  earlierSound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,500,750,60);
  fill(0,0,204);
  textSize(18);
  text(rr2,40,500,750,60);
  fill(100);
  textSize(14);
  text(r2,40,525,600,60);
}
function drawDiehappy(){
  happySound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,580,750,60);
  fill(0,0,204);
  textSize(18);
  text(d,40,580,750,60);
  fill(100);
  textSize(14);
  text(d2,40,604,600,60);
}
function drawDiealone(){
  sadSound.play();
  clickSound.play();
  fill(1000);
  noStroke();
  rect(40,580,750,60);
  fill(0,0,204);
  textSize(18);
  text(dd,40,580,750,60);
  fill(100);
  textSize(14);
  text(dd2,40,604,600,60);
}
