//Declaring and defining all quotes used in the program:
let title = 'How well do I fit into society?';
let title3 = 'According to Google'
let subq1 = 'Do you choose to get married?'
let subq2 = 'Do you choose to have children?'
let subq3 = 'Do you choose to get an education?'
let subq4 = 'Do you choose to get a job?'
let subq5 = 'Do you choose to work until retirement age?'
let subq6 = 'Do you choose to die happy?'
let m = 'On july 1. we finally said yes to each other❤️';
let m2 = 'Around 2 mil. people in Denmark are married right now. 1.6 mil. people over the age of 18 lives alone.';
let s = 'I was so lucky to be able to bring children into this world. No matter what, no matter when, no matter where, I love them always🍼.';
let s2 = 'Today, 21.4% of all danish men and 13.7% of all danish women over the age of 50, do not have children.';
let t = 'I took my masters degree. It was a long and hard battle, but it got me my dream job, and looking back, I would NOT have changed it for a thing📚';
let t2 = 'In February 2020, approx. 46.600 people in the age 15-24, had not completed any education other than primary school in Denmark.';
let j = 'I finally got my dream job!🚀';
let j2 = 'In 2021, 755.000 of the 15-64-year-olds in Denmark were out of the workforce. You are a part of these 755.000 people';
let st = 'I have been employed until the age of 70. I am telling you, it has been a great honor for me.'
let o = 'You chose to retire earlier. In Denmark 24.400 new early retirees were added during 2020, which is the largest increase in over ten years.'
let o1 = 'In Denmark, 55.000 people over the age of 65 have loneliness close to home in everyday life. A total of 380.000 Danes feel very lonely'
let o3 = 'You did it. You died happy. With no regrets and with a life filled with love, work, accomplishments and respect.'


//Preloading all images and sounds:
function preload (){
    //Background:
    img = loadImage("baggrund.png");
    //Marriage:
    img2 = loadImage("marry.png");
    img5 = loadImage("nomarriage.png");
    img7 = loadImage("ring.png");
    //Child:
    img3 = loadImage("child.png");
    img4 = loadImage("baby.png");
    img8 = loadImage("sut.png");
    //Education:
    img6 = loadImage("uni.png");
    //Job:
    img9 = loadImage("job.png");
    //Retirement:
    img10 = loadImage("retire.png");
    //death
    img11 = loadImage("diehappy.png");

    //sound-effects:
    sadSound = loadSound("sad.mp3");
    happySound = loadSound("happy.mp3");
    babySound = loadSound("baby.mp3");
    celebrateSound = loadSound("celebrate.mp3");
    weddingSound = loadSound("wedding.mp3")
    noWork = loadSound("noWork.mp3");
    noMarriage = loadSound("noMarriage.mp3");
    noChild = loadSound("noChild.mp3");
    noEducation = loadSound("noEducation.mp3");
    retirement = loadSound("retirement.mp3");
    death = loadSound("death.mp3");

}
function setup() {
//setting up the canvas, and adding the background (bulletin board):
  createCanvas(1385, 710);
  background(img);
  fill(235, 207, 176);
  rect(1000,0,400,720);

//Making the titles:
  textSize(32);
  textFont('Georgia');
  fill(255);
  stroke(0);
  strokeWeight(4);
  text(title,345,30,420,70);
  textSize(25);
  strokeWeight(3);
  text(title3,455,75,480,70);

//the sub-questions:
  textSize(18);
  text(subq1,1020,42,420,70);
  text(subq2,1020,142,420,70);
  text(subq3,1020,242,420,70);
  text(subq4,1020,342,420,70);
  text(subq5,1020,442,420,70);
  text(subq6,1020,542,420,70);




//Creating the different buttons:
  var button1 = createButton("Get married");
  let col = color(1000);
  button1.style('background-color', col);
  button1.size(150,50);
  button1.position(1030, 70);
  button1.style("font-size", "1em");
  button1.mousePressed(drawMarriage);

  var button11 = createButton("Do not get married");
  let col2 = color(1000);
  button11.style('background-color', col2);
  button11.size(150,50);
  button11.position(1200, 70);
  button11.style("font-size", "1em");
  button11.mousePressed(drawNomarriage);

  var button2 = createButton("Have children");
  let col3 = color(1000);
  button2.style('background-color', col3);
  button2.size(150,50);
  button2.position(1030, 170);
  button2.style("font-size", "1em");
  button2.mousePressed(drawChild);

  var button21 = createButton("Do not have children");
  let col4 = color(1000);
  button21.style('background-color', col4);
  button21.size(150,50);
  button21.position(1200, 170);
  button21.style("font-size", "1em");
  button21.mousePressed(drawNochild);

  var button3 = createButton("Get an education");
  let col5 = color(1000);
  button3.style('background-color', col5);
  button3.size(150,50);
  button3.position(1030, 270);
  button3.style("font-size", "1em");
  button3.mousePressed(drawEducation);

  var button31 = createButton("Do not get an education");
  let col6 = color(1000);
  button31.style('background-color', col6);
  button31.size(150,50);
  button31.position(1200, 270);
  button31.style("font-size", "1em");
  button31.mousePressed(drawNoeducation);

  var button4 = createButton("Get a job");
  let col7 = color(1000);
  button4.style('background-color', col7);
  button4.size(150,50);
  button4.position(1030, 370);
  button4.style("font-size", "1em");
  button4.mousePressed(drawJob);

  var button41 = createButton("Do not get a job");
  let col8 = color(1000);
  button41.style('background-color', col8);
  button41.size(150,50);
  button41.position(1200, 370);
  button41.style("font-size", "1em");
  button41.mousePressed(drawNojob);

  var button5 = createButton("Work until retirement age");
  let col9 = color(1000);
  button5.style('background-color', col9);
  button5.size(150,50);
  button5.position(1030, 470);
  button5.style("font-size", "1em");
  button5.mousePressed(drawStay);

  var button51 = createButton("Retire earlier");
  let col10 = color(1000);
  button51.style('background-color', col10);
  button51.size(150,50);
  button51.position(1200, 470);
  button51.style("font-size", "1em");
  button51.mousePressed(drawDivorce);

  var button6 = createButton("Die happy");
  let col11 = color(1000);
  button6.style('background-color', col11);
  button6.size(150,50);
  button6.position(1030, 570);
  button6.style("font-size", "1em");
  button6.mousePressed(drawDiehappy);

  var button61 = createButton("Die alone");
  let col12 = color(1000);
  button61.style('background-color', col12);
  button61.size(150,50);
  button61.position(1200, 570);
  button61.style("font-size", "1em");
  button61.mousePressed(drawDieregretful);

  var buttonE = createButton("RESTART PAGE!");
  let col13 = color(255,230,0);
  buttonE.style('background-color', col13);
  buttonE.size(200,40);
  buttonE.position(1080, 650);
  buttonE.style("font-size", "1em");
  buttonE.mousePressed(drawRestart);
}

//Making the functions referred to in the buttons:
function drawMarriage(){
  weddingSound.play();
  image(img2,160,48,153,165);
  image(img7,100,30,100,100);
  fill(0);
  noStroke();
  textSize(13);
  text(m,155,203,150,40);
}

function drawNomarriage(){
  sadSound.play();
  noMarriage.play();
  fill(255,0,0);
  noStroke()
  textSize(15);
  text(m2,160,60,150,200);
}

function drawChild(){
  babySound.play();
  image(img3,370,245,120,120);
  image(img8,600,90,100,130);
  fill(0);
  noStroke()
  textSize(13);
  text(s,440,150,185,100);
  image(img4,560,235,110,100);
}

function drawNochild(){
  sadSound.play();
  noChild.play();
  fill(255,0,0);
  noStroke()
  textSize(14);
  text(s2,440,165,185,100);
}

function drawEducation(){
  celebrateSound.play();
  image(img6,15,350,162,170);
  fill(0);
  noStroke()
  textSize(12);
  text(t,40,305,150,100);
}

function drawNoeducation(){
  sadSound.play();
  noEducation.play();
  fill(255,0,0);
  noStroke()
  textSize(15);
  text(t2,40,310,150,200);
}

function drawJob(){
  happySound.play();
  image(img9,245,440,165,185);
  fill(0);
  noStroke()
  textSize(13);
  text(j,245,615,160,100);
}

function drawNojob(){
  sadSound.play();
  noWork.play();
  fill(255,0,0);
  noStroke()
  textSize(17);
  text(j2,245,455,155,200);
}

function drawStay(){
  happySound.play();
  image(img10,550,570,135,150);
  fill(0);
  noStroke()
  textSize(15);
  text(st,500,515,160,200);
}

function drawDivorce(){
  sadSound.play();
  retirement.play();
  fill(255,0,0);
  noStroke()
  textSize(15);
  text(o,495,525,160,120);
}

function drawDiehappy(){
  happySound.play();
  image(img11,760,50,200,200);
  fill(1000);
  noStroke()
  textSize(15);
  text(o3,705,370,150,200);
}

function drawDieregretful(){
  sadSound.play();
  death.play();
  image(img11,760,50,200,200);
  fill(255,0,0);
  noStroke()
  textSize(15);
  text(o1,705,370,150,200);

}
function drawRestart(){
  location.reload();
}
