//declaring the varibales (for the buttons):
var button;
var button2;
var button3;
var button4;

// declaring and assigning the variables "t" and "s" for the texts:
var t = 'Dear student at AU. Please complete the following form';
var s = 'By agreeing to these Terms of Service, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site. You may not use our products for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws). You must not transmit any worms or viruses or any code of a destructive nature. A breach or violation of any of the Terms will result in an immediate termination of your Services.We are not responsible if information made available on this site is not accurate, complete or current. The material on this site is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or more timely sources of information. Any reliance on the material on this site is at your own risk. This site may contain certain historical information. Historical information, necessarily, is not current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on our site. You agree that it is your responsibility to monitor changes to our site. We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free. We do not warrant that the results that may be obtained from the use of the service will be accurate or reliable. You agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, without notice to you. You expressly agree that your use of, or inability to use, the service is at your sole risk. The service and all products and services delivered to you through the service are (except as expressly stated by us) provided as is and as available for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement. In no case shall The Longest Thread, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any products procured using the service, or for any other claim related in any way to your use of the service or any product, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content (or product) posted, transmitted, or otherwise made available via the service, even if advised of their possibility. Because some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be limited to the maximum extent permitted by law. You give us your full consent to capture your face at all times, when this local computer is in use. You allow us to access all information, contacts, attachments, files, pictures, messages and your browser history when you use this local computer. You allow us to call a random friend from your contacts every other saturday. You give us the permission to track this local computer at all times. We believe that privacy is not important to you. you have no privacy with us. If we can use any of your details to legally make a profit, we probably will. Some of the Cookies are managed by third parties who have their own policies regarding how they collect and store information. Such practices are not covered by our Privacy Policy and we do not have any control over them.';

var r = 'haha';

let capture;

function setup(){
//setting up the canvas:
 createCanvas(1400, 220);
 background(1000);

//creating the first button:
 button = createButton("Yes, I agree to all terms and conditions")
 button.size(300,40);
 button.position(320, 250);
 button.style("font-size", "1em");

//creating the second button:
 button3 = createButton("No, I do not agree")
 button3.size(150,40);
 button3.position(680, 250);
 button3.style("font-size", "1em");

//making the function faceCapture run, when "button" is pressed:
 button.mousePressed(faceCapture);

//making the function tooBad run, when "button3" is pressed:
 button3.mousePressed(tooBad);

//writing the title:
  textSize(21);
  text(t, 4, 4, 600, 20);

//writing the terms and conditions:
  textSize(10);
  fill(150);
  text(s, 4, 40, 1400, 400);
}

function faceCapture(){
//hiding "button" and "button3" when "button2" is clicked:
   button.hide();
   button3.hide();

//making a rectangle that will hide the texts:
  fill(1000);
  rect(0,0,1500,341);

//web cam capture
   capture = createCapture(VIDEO);
   capture.size(600, 600);

//creating the scam-text as a button:
   button2 = createButton("THIS IS A SCAM. SMILE FOR THE CAMERA BEAUTIFUL! 🤡⚠️ We are watching you right now, and just wanted to inform you, that we are so exited to follow you wherever you go! PS. we can't wait to talk to your friends every other saturday!👹📸📞");
   button2.position(620,350);
   button2.style('color:red');
   button2.size(720,235);
   button2.style("font-size", "2em");
}

function tooBad(){
//hiding both buttons:
    button.hide();
    button3.hide();

//making a rectangle that will hide the texts:
    fill(1000);
    rect(0,0,1500,341);

//creating the text as a button:
    button4 = createButton("Of course you agree dear... Don't you know that you can't acces anything in the world of software, if you don't agree to the terms and conditions? Please refresh the page in order to agree.")
    button4.size(700,150);
    button4.position(300, 250);
    button4.style("font-size", "1.4em");


}
