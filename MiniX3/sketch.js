// declaring a variable "angle", and assigning it:
let angle = 0;

// declaring "s" and "stars" and assigning them:
let s = 'Saturn making it look like something is about to happen...';

let stars = '*';

// declaring "k" and assigning different numbers used further in the code:
let k = [102, 255, 10, 100, 200];

// setting up a canvas:
function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
// giving the background a blue sky-color
  background(153, 204, k[1]);

// drawing the small stars:
  fill(k[1], k[1], k[0]);
  noStroke();
  textSize(60);
  text(stars, 290, k[1], k[0]);
  text(stars, 230, k[1], k[0]);
  text(stars, 980, k[1], k[0]);

// drawing the big starts:
  textSize(110);
  text(stars, 890, k[1], k[0])
  text(stars, 390, 655, k[0])

// drawing the text and deciding the position of the text
  noStroke();
  fill(1000);
  textSize(22);
  text(s, 440, k[3], 600, k[4]);

// translating/moving the canvas to the middle of the screen 
// and drawing the sun 
  translate(width/2, height/2);
    rotate(angle);
    fill(k[1], k[1], k[0]);
    ellipse(k[2], k[2], k[4], k[4]);


// translating once again and drawing Saturn 
// to make it rotate around the sun 
// the new translation is affected by the previous translation
  translate(width/6, height/6);
    rotate(angle);
    strokeWeight(4);
    stroke(212, 162, 47);
    fill(230, 196, 116);
    ellipse(k[2], k[2], k[3], k[3]);
// including the ring in Saturn in the rotation:
    strokeWeight(6);
    stroke(k[1], 128, 0);
    noFill();
    ellipse(k[2], k[2], k[4], 20);

// adding 0,017 to the value of "angle" in order to control
// the speed of both Saturn and the sun
// or in other words: choose how many degrees the planets move pr. frame:
  angle += 0.017;
}
