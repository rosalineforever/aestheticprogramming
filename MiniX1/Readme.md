### MiniX1

![](caterpillar.png)

## A link to a Youtube-video of my work:
(https://www.youtube.com/watch?v=PoVNsNXK1Q8)

## The URL that links to my work:
https://rosalineforever.gitlab.io/aestheticprogramming/MiniX1/

## Link to my code:
https://gitlab.com/rosalineforever/aestheticprogramming/-/blob/main/MiniX1/sketch.js

### ReadMe 

In my first MiniX, I have had a focus on creating something that is recognizable from the real world - or maybe more from the fantasy world. I chose to produce a green caterpillar out of the geometric shape ‘ellipse’. The caterpillar consists of different sizes and shades of ellipses, that I have put together to form the creature. I also added some different colors of small circles to the body of the caterpillar, to bring it more life and colorfulness. Besides that, I have created what is supposed to look like a moving full moon in the background (the movement of the moon can be seen in the linked video). The background - which is everything but the caterpillar and the moon - consists of the syntaxes 'background()' 'and rect()' - the sky is the dark blue color, while the ground is the brown. The syntaxes I have used besides ‘function setup()’, ‘createCanvas()’ and ‘function draw() are; defining ‘x’ and ‘y’ in order to make the moon move with ‘x= x+0.15;’. Then I have used the before mentioned ‘ellipse()’, ‘rect()’ and ‘fill()’. All the mentioned syntaxes are completely new to me, since this is my first coding-assignment, so my MiniX1 is very simple and I have focused on creating something from scratch. 

I would say that my first independent coding experience has been a bit challenging. I did not want to just copy something from www.p5.js.org and not be fully able to describe what the different syntaxes and parameters does or means. Therefore I have stuck to pretty simple syntaxes that I understand, and would be able to argue. A thing that I was struggling with was the syntax "fill()", since I was not aware in the start, that what comes after this syntax, will be affected by it, and that you have to use the syntax again multiple times, if you want to change the colors throughout the coding. 
The coding process reminds me alot about the math-program ‘Maple’ that I used in the gymnasium. While using Maple you have to be very exact and extremely detailed about what you write, in order to get a successful result out of it. To me, the same counts for coding; if you are missing one single curly bracket, the program will not be able to run, and the canvas will be blank. 
I feel like the difference between writing and coding is this sense of detail that is required for programming in general. When writing, it is easier to refrain from things that you find difficult, by changing the subject or arguing your way out of it. This is not possible when coding. If something is not written down correctly or you forget a “;” or a “}”, your canvas will show an error - or in most cases: be blank. Though, there is the ReadMe as a part of the assignment, and this element leaves place for informing the reader, what you might have been struggling with, or maybe have not been able to solve/perform. 
Things I would like to learn more about concerning programming, is making more things/objects move at the same time - and in different directions. Also I would like to know syntaxes that make the coding process more simple, so it does not have to fill 110 lines, like it does right now.  


