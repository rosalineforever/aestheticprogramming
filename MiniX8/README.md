# MiniX8

Provide provocation/problematize: mental health (stress among students). 



## Link to code:
https://gitlab.com/rosalineforever/aestheticprogramming/-/blob/main/MiniX8/sketch.js

## Link to RunMe:
https://rosalineforever.gitlab.io/aestheticprogramming/MiniX8

![](todo.png)

## ReadMe

### "A to do list"
We decided to make a “To Do List” addressing the pressure of being productive - with special focus on mental health of young people. It can be hard maintaining good grades, family and social life, working out, eating healthy, romantic relationships, appearance, taking care of mental health, therapy, work, making money and staying up to date with social media. All these are just examples, but trying to fit all of those things into the 24 hours of the day can be stressful and cause anxiety. We think there is a huge pressure on young people staying “on track” with all these expectations from both society, their surroundings, but also themselves.
ToDo lists are helpful with putting unfinished tasks and goals from your mind to a concrete list on your phone/computer/paper. It gives you a clear overview over the tasks you have to do and makes it easier to prioritize them. It can be a burden feeling like you have many things undone. The act of ticking off a task from a ToDo list is shown to make us feel good and potentially release dopamine. 

### The program
As written above, this week´s minx is a “To Do List”. We started out creating our program by first making our main stech.js-file and then creating two other JSON-files; resp. called “humans.json” and “actions.json”. As the names of the files might reveal, the human.json-file contains all the subjects/humans that we chose to implement in our coding, while the action.json-file contains all the actions possible - or all the “steps” in a potential todo list. In our sketch.js-file we started out by preloading the data from both JSON-files. Then we set up the canvas, wrote the title for the artwork and made a for-loop in order to show six different actions each time the button is clicked. Lastly we made a button that reloads the page, each time the button is being clicked. We had a problem with making the text be inside of the canvas + making the text start from further out on the x-axe. 
 
### Analyzis of our work 
Our e-lit work consists of short sentences in the setup of a To-do list. Our program does not speak out loud, so it is very much up to the reader/viewer of the program to interpret our program with their own “inner voice”. Still, the language we chose for the program has a meaning, and the words we chose are not just random - there is a thought behind it. We wrote the text in a modern, teenage-type style so it relates to the audience we are aiming for. The words would not make any sense on their own, but put together like this it becomes litterature, even if it is not read out loud like a sound poem:
“To attempt to understand a “text” like a sound poem requires recognition that all language consists of closed systems of discrete semiotic elements, and that a meaning is organized out of differences between elements that are meaningless in themselves” (Vocable Code, p.19) 
As written above our e-lit work don´t speak out loud, but you can argue that it still has a voice inside your head. All the thoughts swimming around in your head about what to do today or just in general, are being expressed/ or made fysical in the “To Do List”. So you can kind of say that the throughs have a voice, telling you to do certain things. 
 
### Our work in terms of Vocable Code
Vocable code is a way of using code as a language, to create language statements and the use of voices. In our E-lit we want to create awareness of a busy life, using a “To Do List”, using different kinds of actions to show how busy and stressful a normal person's everyday life is. This is why we use language as a way of showing this, but also coding elements such as the syntax Random to show how many thoughts and actions you think about in one day.

### Why electronic literature is the best media our idea
Having your To-do list on your  phone or computer is convenient because it’s very accessible throughout the day. However the constant reminder that there is something that you have to do, can be very stressful and draining in the long run. 
We feel like electronic literature is the best media for our idea, because it relates to the way some people organize their daily lives using software.



### References:
- The Coding Train on Youtube https://youtube.com/playlist?list=RDCMUCvjgXvBlbQiydffZU7m1_aw&playnext=1
- Harvard Business Review article (“Why We Continue to Rely on (and Love) To-Do Lists”) by Vasundhara Sawhney: https://hbr.org/2022/01/why-we-continue-to-rely-on-and-love-to-do-lists
- Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38.

