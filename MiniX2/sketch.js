//declaring r, g and b
//will be declared as random colors within function setup:
let r, g, b;

function setup(){
//creating the canvas and setting the background-color 
createCanvas(windowWidth, windowHeight);
background(0, 500, 500);

// setting the frame rate - deciding how many frames are ran through every second
// Inizializing the values of r, g and b, to be from 1 to 150 on the rgb-color-scheme
frameRate(1000);
r = random(150);
g = random(150);
b = random(150);
}

// making the brush for my canvas:
// there is an "if-statement" and a conditional statement
function draw(){
  fill('black');
  if(mouseIsPressed){
  ellipse(mouseX, mouseY, 2, 2);
}

// making the circle for the first emoji:
// giving it random colors - the last argument is the transparency 
fill(r, g, b, 1.7);
strokeWeight(6);
ellipse(400, 500, 400, 400);

// making the eyes (right emoji):
  fill(1000);
  ellipse(320, 480, 60, 60);
  ellipse(470, 480, 60, 60);
  fill(0);
  // Making the pupils:
  ellipse(330, 480, 20, 20);
  ellipse(480, 480, 20, 20);
  // the white in the pupils:
  fill(1000);
  ellipse(335, 480, 12, 12);
  ellipse(485, 480, 12, 12);
  // making the second emoji
  fill(r, g, b, 1.7);
  strokeWeight(6);
  ellipse(900, 500, 400, 400);

// making the eyes (left emoji):
  fill(1000);
  ellipse(820, 480, 60, 60);
  ellipse(970, 480, 60, 60);
  fill(0);
  // Making the pupils:
  fill(0);
  ellipse(810, 480, 20, 20);
  ellipse(960, 480, 20, 20);
  // the white in the pupils:
  fill(1000);
  ellipse(805, 480, 12, 12);
  ellipse(955, 480, 12, 12);







}
