# **MiniX9**
_By Marie, Nanna, Thelma, Maria and Rosa_

### Our First flowchart: _'The circle of life'_
![](environment.png)

### Our second flowchart: _'Dear world'_
![](earth.png)

## ReadMe

### Difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level
What we have been struggling with the most, and what has also been discussed the most in our group in the process of making the flowcharts, was the question about how advanced, detailed and maybe how “academic” the flowchart should be. In the preparation process of both flowcharts, we started out with outlining and writing down, in broad strokes, what was necessary to implement in our flowcharts, and what was not that important to include, in order for the viewer to understand the process of our program. Considering that we were not completely sure what exact syntaxes and functions would fit into our final source code in our final MiniX that we were brainstorming on, we assessed to write down keywords and potential syntaxes to be used in the program, in order to perform what we had in mind. This was also a bit of a struggle. The element of how “academic” the text in the flowchart should be, was as mentioned, a bit of a struggle for us. Whether terms and concepts like “variables”, “functions”, “frames” and “loops” was something we had divided opinions about, but we compromised and added terms that would be understandable for e.g. a teacher in our school who was not that familiar with the subject or with programming. We often returned to this “fictional person” and re-evaluated our text accordingly. 

### The technical challenges facing the two ideas and how we address these
For our first idea: “The Environmental Flowchart”, we face the technical challenge that is creating sliders with a very specific outcome, and using emojis as the sliders button. None of us have experimented with this before, so it might be difficult at first when we have to figure out how to get the outcome we plan to have. We got inspired by Albin´s minix5, where we had to make a program that consisted of something auto-generated. He used sliders in his program, so that's where we got the idea of using sliders to affect the fictional earth in our program. To overcome the difficulties of trying and using sliders, we have decided that all of us have to check it out and try using it at a lower level, before making the final code. To properly understand the difficulties, we will also seek information about sliders and using emojis as buttons, by reading and watching videos.   
For our second idea we did a game with the concept of the expectations on how people should live their life. In the game you make choices in a virtual life where there is only one “right” choice in each step. It’s a satire on the pressure you feel to make the right choices at the right times in life. So the technical challenges in this idea would be how to structure the if-statements, so it displays in the right order, and making sure the buttons are doing the right thing. It has many steps so it will probably have a lot of lines of code but not too complex.

### Ways in which the individual and the group flowcharts we produce are useful
In our group, we all have different ways of writing down source code, commenting on the code in Atom, brainstorming and performing a piece of work in general. This exact thing is reflected to some extent, when comparing all of the group members' individual flowcharts to the flowchart in our mutual group work. Some of us made our individual flowchart before working on the group work, and some did it after - which definitely has affected the way in which we have prepared our own flowchart. Some got inspiration from the mutual work, others from external sources and some made it without much inspiration from anything other than what was taught and touched upon in class. We all find it very useful to rethink our code and procedures on a communicational level. In order to make this useful, we found it necessary to understand all elements of the code we have produced.


## My individual flowchart 
I chose to make a flowchart of the MiniX7, since this MiniX is the the one with the most inputs, functions, syntaxes, dowloadnings - and overall just the longest code. 
Link to MiniX7: 
https://gitlab.com/rosalineforever/aestheticprogramming/-/blob/main/MiniX7/README.md
![](myflowchart.png)
